#!/usr/bin/env python
##################################
# URL Parser for Marin Software  #
#                                #
#    Elias Zeidan August 2013    #
##################################

"""
Version 0.12
This parser should do the following:

 1. Take as input a file list of URLs, one per line, and 
 2. Turn that list URLs into an array
    [url0, url1, url2, url3, ..., urln-1]
 3. For every URL in the array, pull out the different key-value pairs
    into a dict 
    [{key00: value00, key01: value01, key0n: value0n},
     {key10: value10, key11: value11}...
     {...,keynn: valuenn}]
 4. Compare the input URLs to an expected value - currently looking
    for exact matches.

# TODO: Add regex matching for query strings
# TODO: Add Django web UI. Project exists on home computer
"""


import sys, re

def input_and_read_file(filename=None, content=None):
    """
    Reads a list of URLs
    returns an array of query strings from these URLs
    """
    if filename != None:
        urls = open(filename).read().splitlines()
    elif content != None:
        urls = content
    return urls

def parse_out_values(urls):
    """
    Strips query strings from URLs
    input: http://www.example.com?abc=123&jkl=def
    ouput: abc=123&jkl=def
    Won't work properly for URLs with multiple query strings...
    this shouldn't happen anyway.
    """
    queries = []
    # strip out query strings
    if len(urls) != 1:
        for url in urls:
            # DEBUG: print url
            if url[url.find('?')+1:] != url:
                # DEBUG: print url[url.find('?')+1:]
                queries.append(url[url.find('?')+1:])
            else:
                # DEBUG: print url[url.find('?')+1:]
                queries.append('NULL=NULL')
                # DEBUG: print url
    else:
        urls = urls[urls.find('?')+1:]
    # DEBUG: print urls, len([urls])
    # DEBUG: print queries
    dicts = []
    if len(urls) == 1:
        dicts.append(dict(query.split('=') for query in
    urls.split('&'))) 
        return dicts
    else:
        for i in range(len(queries)):
            # DEBUG: print queries[i]
            dicts.append(dict(query.split('=') for query in
    queries[i].split('&'))) 
        return dicts

def expected_values(expected):

    """
    Assumes that all URLs have similar parameters, so can use one
    "good" URL to determine "expected" value
    """
    urls = expected[expected.find('?')+1:]
        
    # DEBUG: print urls, len([urls])
    # DEBUG: print queries
    return  dict(query.split('=') for query in urls.split('&'))

def compare(urls, expected):

    """
    Input:
        urls: filename of list of URLs
        expected: URL with expected params

    1 - Parse out query strings using parse_out_values()
    2 - compare to expected
    3 - Return 'True'/'False' result

    # TODO: Add regex matching for query strings

    Returns:
        List of 'True'/'False' per URL
    """
    
    urls = input_and_read_file(filename=urls)
    queries = parse_out_values(urls)

    expected = expected_values(expected)

    results = []

    print "Expected parameter pairs: \n\t %s" % expected
    
    for i in range(len(queries)):
        results.append(expected == queries[i])

    for i in range(len(queries)):
        for j in range(len(queries[i])):
            for k in range(len(expected)):
                print "%d: %s=%s\t%s=%s" %(i+1, queries[i].keys()[j],
        queries[i].values()[j], expected.keys()[k], expected.values()[k])



    for i in range(len(results)):
        print "Results:"
        print "%s : \t %s\n" %(urls[i], results[i]) 

def test():

    print "***Test 1: print parameters***\n"
    urls = input_and_read_file(filename='urls.txt')
    print urls
    print parse_out_values(urls)

    print "***Test 2: compare parameters***\n"
    compare('urls.txt',
    'http://www.example.com?utm_content={ifsearch:s}{ifcontent:c}a1B2c3D4|pcrid|{creative}|pkw||pmt|&marin=mss&stuff=more_stuff')   


def main():
    print compare(urls, expected)

if __name__ == "__main__":
    if len(sys.argv) == 3:
        urls = str(sys.argv[1])
        expected = str(sys.argv[2])
        main()
    else:
        print "Uh oh! This requires two parameters:\n\t1 - the URL\
    file (txt)\n\t2  - a \'correct\' URL for comparison, in the\
    following order:\n\nparser.py <url_file> <expected>"   
